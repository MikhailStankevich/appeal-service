<?php

namespace Stan\Appeals\Handler;

/**
 * Interface HandlerInterface
 *
 * @package Stan\Appeals\Handler
 */
interface HandlerInterface
{
    /**
     * @return void
     */
    public function execute(): void;
}