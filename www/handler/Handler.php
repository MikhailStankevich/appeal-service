<?php

namespace Stan\Appeals\Handler;

use LeadGenerator\Lead;
use Stan\Appeals\Logger\Logger;

/**
 * Class Handler
 */
class Handler implements HandlerInterface {

    /**
     * @var Lead
     */
    private Lead $lead;

    /**
     * @param Lead $lead
     */
    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
    }

    /**
     * @return Lead
     */
    public function getLead(): Lead
    {
        return $this->lead;
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        sleep(2);
        $logData = $this->lead->id . ' | ' . $this->lead->categoryName . ' | ' . date('Y-m-d H:i:s');
        $logger = new Logger();
        $logger->write($logData);
    }
}