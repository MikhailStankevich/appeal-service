<?php

namespace Stan\Appeals\Worker;

use Pheanstalk\Pheanstalk;
use Stan\Appeals\Handler\Handler;

/**
 * Class Worker
 */
class Worker implements WorkerInterface
{
    /**
     * @var Pheanstalk
     */
    private Pheanstalk $pheanstalk;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pheanstalk = Pheanstalk::create(HOST);
    }

    /**
     * @return Pheanstalk
     */
    public function getPheanstalk(): Pheanstalk
    {
        return $this->pheanstalk;
    }

    /**
     * @return void
     */
    public function run(): void
    {
        // Чтение очереди beanstalkd
        while (true) {
            // Получить задачу из очереди, если она готова
            $job = $this->pheanstalk
                ->watch(LEADS_QUEUE)
                ->ignore(DEFAULT_QUEUE)
                ->reserve();

            try {
                $lead = json_decode($job->getData());

                $handler = new Handler($lead);
                $handler->execute();

                // Удалить задачу из очереди
                $this->pheanstalk->delete($job);
            } catch (\Exception $error) {
                // Освободить задачу
                $this->pheanstalk->release($job);
            }
        }
    }
}
