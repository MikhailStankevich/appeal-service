<?php

namespace Stan\Appeals\Worker;

/**
 * Interface WorkerInterface
 *
 * @package Stan\Appeals\Worker
 */
interface WorkerInterface
{
    /**
     * @return void
     */
    public function run(): void;
}