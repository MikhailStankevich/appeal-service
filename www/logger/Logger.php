<?php

namespace Stan\Appeals\Logger;

/**
 * Class Logger
 */
class Logger implements LoggerInterface {

    /**
     * @param string $data
     * @return void
     */
    public function write(string $data): void
    {
        $fh = fopen('log.txt', 'a');
        fwrite($fh, $data. PHP_EOL);
        fclose($fh);
    }
}