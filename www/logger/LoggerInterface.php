<?php

namespace Stan\Appeals\Logger;

/**
 * Interface LoggerInterface
 *
 * @package Stan\Appeals\Logger
 */
interface LoggerInterface
{
    /**
     * @param string $data
     * @return void
     */
    public function write(string $data): void;
}