<?php

namespace Stan\Appeals\Sender;

/**
 * Interface SenderInterface
 *
 * @package Stan\Appeals\Sender
 */
interface SenderInterface
{
    /**
     * @return void
     */
    public function execute(): void;
}