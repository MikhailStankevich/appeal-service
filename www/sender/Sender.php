<?php

namespace Stan\Appeals\Sender;

use Pheanstalk\Pheanstalk;
use LeadGenerator\Lead;

/**
 * Class Sender
 */
class Sender implements SenderInterface
{
    /**
     * @var Lead
     */
    private Lead $lead;

    /**
     * @var Pheanstalk
     */
    private Pheanstalk $pheanstalk;

    /**
     * @param Lead $lead
     */
    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
        $this->pheanstalk = Pheanstalk::create(HOST);
    }

    /**
     * @return Lead
     */
    public function getLead(): Lead
    {
        return $this->lead;
    }

    /**
     * @return Pheanstalk
     */
    public function getPheanstalk(): Pheanstalk
    {
        return $this->pheanstalk;
    }

    /**
     * @return void
     */
    public function execute(): void
    {
        $this->pheanstalk
            ->useTube(LEADS_QUEUE)
            ->put(json_encode($this->lead));
    }
}
