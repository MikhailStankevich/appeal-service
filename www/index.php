<?php

require 'vendor/autoload.php';

use LeadGenerator\Generator;
use LeadGenerator\Lead;
use Stan\Appeals\Worker\Worker;
use Stan\Appeals\Sender\Sender;

const HOST = '127.0.0.1';
const DEFAULT_QUEUE = 'default';
const LEADS_QUEUE = 'leads_queue';
const LEADS_COUNT = 10000;

// Создаем и запускаем воркеры
for ($i = 0; $i < LEADS_COUNT / 100; $i++) {
    $worker = new Worker();
    $worker->run();
}

// Генерируем заявки и отправляем их в очередь
$generator = new Generator();
$generator->generateLeads(LEADS_COUNT, function (Lead $lead) {
    $sender = new Sender($lead);
    $sender->execute();
});